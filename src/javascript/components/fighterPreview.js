import { createElement } from '../helpers/domHelper';
import '../../styles/fighterPreview.css'
import attack from '../../../resources/icons/attack.png'
import defense from '../../../resources/icons/defense.png'
import health from '../../../resources/icons/health.png'

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterDetailsWrapper = createElement({
      tagName: 'div',
      className: 'fighter-details__wrapper'
    });

    const fighterName = createElement({
      tagName: 'h2',
      className: 'fighter-details__title'
    });
    fighterName.innerText = fighter.name;
    const fighterDetail = createElement({
      tagName: 'div',
      className: 'fighter-details__stats'
    });
    const healthDetail = createFighterDetailInfo('health', fighter.health);
    const attackDetail = createFighterDetailInfo('attack', fighter.attack);
    const defenseDetail = createFighterDetailInfo('defense', fighter.defense);
    fighterDetail.append(healthDetail, attackDetail, defenseDetail);
    fighterDetailsWrapper.append(fighterName, fighterDetail);
    fighterElement.append(fighterImg, fighterDetailsWrapper);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterDetailInfo(label, value) {
  const detail = createElement({
    tagName: 'div',
    className: 'fighter-details__stat-cell'
  });

  const icon = createDetailIcon(label);
  const valueElement = createElement({
    tagName: 'p'
  });
  valueElement.innerText = value;
  detail.append( icon, valueElement);
  return detail;
}
function createDetailIcon(label) {
  return createElement({
    tagName: 'img',
    className: 'fighter-details__icon',
    attributes: {
      src: label==='defense'? defense:label==='attack'? attack: health ,
      alt: label
    }
  });
}