import { callApi } from '../helpers/apiHelper';
import {getFighterInfo} from "../components/fighterSelector";

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
     return getFighterInfo(id);
  }
}

export const fighterService = new FighterService();
